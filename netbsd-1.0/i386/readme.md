

Image with networking in QEMU 4.1.1 

qemu-system-i386 \
-hda netbsd-1.0.vmdk \
-netdev user,id=net0,hostfwd=tcp::20023-:23\
-device ne2k_isa,netdev=net0,irq=10,iobase=0x320 \
-m 64 \
-no-reboot \
-rtc base=localtime \
-k en-us

