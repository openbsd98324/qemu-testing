# qemu-testing

## Content

qemu and qcow2  for testing

## Create a qcow2 file

zcat devuan_ascii_2.0.0_amd64_qemu.qcow2.gz > devuan_ascii_2.0.0_amd64_qemu.qcow2



## Create an installation of Netbsd 

nconfig create cow

mv disk.img.qcow2 qemu-testing-netbsd-amd64-9.1.img.qcow2 


To install: 

qemu-system-x86_64 -cdrom NetBSD-9.1-amd64.iso   -hda qemu-testing-netbsd-amd64-9.1.img.qcow2  -boot once=d



To boot: 

qemu-system-x86_64     -hda qemu-testing-netbsd-amd64-9.1.img.qcow2  



## NetBSD running over SSH / Qemu 

![](media/qemu-curses-netbsd-9.1-over-ssh-init.png)
![](media/qemu-curses-netbsd-9.1-over-ssh-install.png)
![](media/qemu-curses-netbsd-9.1-over-ssh-vdisk.png)
![](media/qemu-curses-netbsd-9.1-over-ssh1.png)
![](media/qemu-curses-netbsd-9.1-over-ssh2.png)
![](media/qemu-curses-netbsd-9.1-over-ssh3.png)

Raw image: https://gitlab.com/openbsd98324/qemu-testing/-/raw/main/netbsd-amd64-9.1/5-qemu-ssh/1639211790F1-vdisk-netbsd-9.1-amd64.img.gz



